import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
from models import Net

import data
import utils
from tqdm import tqdm
import os
import pandas as pd

# Parameters
best_model_path = './logs/rakuten_model_1'  # path to load the best model
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")  # sets device for model and PyTorch tensors
dropout = 0.5  # dropout ratio
nb_classes = 19  # 19 colors to classify
out_dim = 1024  # dim of the linear layer after the resnet
hidden_dim = 512  # dim of the LSTM hidden states

num_workers = 1  # number of cpu cores used for data-loading
embedding_method = 'fasttext'  # The embedding method used


def predict():
    """
    Evaluation.
    """

    with torch.no_grad():

        # import dataloaders
        train_loader, valid_loader, test_loader, vocab = data.load_colors_datasets(valid_ratio=0.2, batch_size=64,
                                                                                   num_workers=num_workers, images_dir=None,
                                                                                   dataset_dir=None, train_augment_transforms=None,
                                                                                   embedding_method=embedding_method)

        if embedding_method not in ['bert', 'fasttext']:
            # Get the size of vocabulary
            vocab_size = len(vocab)
        else:
            vocab_size = None

        # Load the best encoder
        net = Net(out_dim, hidden_dim, vocab_size, dropout=0.5,
                  embedding_method='fasttext', embed_dim=None,
                  nb_classes=nb_classes)
        net.load_state_dict(torch.load(os.path.join(best_model_path, 'best_model.pt')))
        net.eval()

        # Move to GPU, if available
        net = net.to(device)

        decoded_preds_arr = []
        idx_arr = []

        # Batches
        for i, (img, text, text_len, idx) in enumerate(tqdm(test_loader)):

            # Move to GPU, if available
            img = img.to(device)
            text = text.to(device)
            text_len = text_len.to(device)
            idx = idx.to(device)

            # Forward prop.
            out = net(img, text)

            # Compute predictions
            predicted_targets = torch.round(out)

            # Retrieve the lists of colors from the predictions
            decoded_preds_arr_batch = utils.ont_hot_decoder(predicted_targets.cpu().numpy()).tolist()
            idx_arr_batch = idx.cpu().numpy().tolist()
            decoded_preds_arr.extend(decoded_preds_arr_batch)
            idx_arr.extend(idx_arr_batch)

        # Storing the results in a DataFrame
        result_df = pd.read_csv(os.path.join('../Data', 'X_test.csv'), index_col=0)
        result_df['color_tags'] = decoded_preds_arr
        result_df['idxs'] = idx_arr
        result_df = result_df.drop(['image_file_name', 'item_name', 'item_caption'], axis=1)
        # Sort to retrieve the same initial order (that has been modified because of sequence padding)
        result_df = result_df.sort_values(by="idxs")
        result_df.to_csv('./logs/predictions.csv')  # save the DataFrame

    return predicted_targets, result_df


if __name__ == '__main__':
    predict()
