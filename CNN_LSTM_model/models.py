import torch
from torch import nn
import torchvision
from transformers import BertModel, BertJapaneseTokenizer
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence, PackedSequence, pad_sequence

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class ImageNet(nn.Module):
    """
    Model for the images // pretrained : resnet without the fc at the end
    """

    def __init__(self, out_dim=1024):
        super(ImageNet, self).__init__()

        resnet = torchvision.models.resnet18(pretrained=True)  # pretrained ImageNet ResNet-18
        self.resnet = resnet
        fc_input_size = self.resnet.fc.in_features
        self.resnet.fc = nn.Linear(in_features=fc_input_size, out_features=out_dim, bias=True)

    def forward(self, img):
        """
        Forward propagation.
        """
        out = self.resnet(img)

        return out


class TextNet(nn.Module):
    """
    Model for the textual data.
    """

    def __init__(self, hidden_dim, vocab_size, dropout=0.5,
                 embedding_method='fasttext', embed_dim=None):
        """
        Args:
            hidden_dim : int
                size of the hidden states and cell states
            dropout : float
                dropout ratio
            embedding_method : str
                embedding method used. One of ['fasttext', 'bert', 'other']
            embed_dim : int
                embedding size (optional, only if embedding_method != 'fasttext' or 'bert')
            vocab_size : int
                size of the vocabulary used (optional, only if embedding_method != 'fasttext' or 'bert')
        """

        super(TextNet, self).__init__()

        self.hidden_dim = hidden_dim
        self.dropout = dropout
        self.embedding_method = embedding_method
        self.vocab_size = vocab_size

        if self.embedding_method == 'bert':
            # Load pre-trained bert model (weights)
            self.embedding = BertModel.from_pretrained("../Embeddings/bert-base-japanese")
            self.embed_dim = 768
        elif self.embedding_method == 'fasttext':
            self.embed_dim = 300
        else:
            if embed_dim is not None:
                self.embedding = nn.Embedding(vocab_size, embed_dim)  # embedding layer
            else:
                raise ValueError('embedding dim must be provided')

        self.dropout = nn.Dropout(p=self.dropout)

        # LSTM layer
        self.lstm = nn.LSTM(self.embed_dim,
                            hidden_dim,
                            num_layers=1,
                            batch_first=True)

    def forward(self, encoded_text):
        """
        Forward propagation.
        """

        # Embedding
        if self.embedding_method == 'bert':
            with torch.no_grad():
                embeddings = self.embedding(**encoded_text)[0]  # (batch_size, max_caption_length, embed_dim)
        elif self.embedding_method == "fasttext":
            embeddings = encoded_text
        else:
            embeddings = self.embedding(encoded_text)

        # Initialize LSTM state
        packed_output, (h, c) = self.lstm(embeddings)

        return h


class Net(nn.Module):
    """
    Classification model
    """

    def __init__(self, out_dim, hidden_dim, vocab_size, dropout=0.5,
                 embedding_method='fasttext', embed_dim=None,
                 nb_classes=19):

        super(Net, self).__init__()

        self.out_dim = out_dim
        self.nb_classes = nb_classes
        self.hidden_dim = hidden_dim
        self.dropout = dropout
        self.embedding_method = embedding_method
        self.vocab_size = vocab_size

        self.ImageNet = ImageNet(out_dim=out_dim)
        self.TextNet = TextNet(hidden_dim, vocab_size, dropout, embedding_method, embed_dim)
        self.fc = nn.Linear(in_features=(out_dim + hidden_dim), out_features=nb_classes, bias=True)
        self.sigm = nn.Sigmoid()

    def forward(self, img, encoded_text):
        """
        Forward propagation.
        """
        out_img = self.ImageNet(img)
        out_txt = self.TextNet(encoded_text)
        out_txt = out_txt.view(out_txt.size()[1], -1)
        print(out_img.size())
        print(out_txt.size())
        in_fc = torch.cat((out_img, out_txt), dim=1)
        out = self.fc(in_fc)
        out = self.sigm(out)

        return out
