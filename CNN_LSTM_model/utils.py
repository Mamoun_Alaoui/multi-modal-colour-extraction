import torch
import ast
import os
import pandas as pd
import numpy as np

import re
from sklearn.preprocessing import MultiLabelBinarizer
import MeCab
import fasttext.util
import spacy  # Download the japanese spacy language model: python -m spacy download ja_core_news_sm or python3 -m etc.


# Loading the fasttext model to compute embeddings. Download the bin file from https://fasttext.cc/docs/en/crawl-vectors.html
ft = fasttext.load_model('../Embeddings/cc.ja.300.bin')

# The list of colors used to extract relevant part of the names + captions
colors_list_ja = ['イエロー', 'オレンジ', 'カーキ', 'グリーン', 'グレー', 'ゴールド', 'シルバー', 'ネイビー',
                  'パープル', 'ピンク', 'ブラウン', 'ブラック', 'ブルゴーニュ', 'ブルー', 'ベージュ', 'ホワイト',
                  'レッド', 'ワインレッド', '白い', '紫の', '緑', '複数の色', '褐色', '赤', '透明性', '銀', '青い',
                  '黄', '黒']

# Useful dict to extract the colors from one hot vectors
index_to_colors = {0: 'Beige', 1: 'Black', 2: 'Blue', 3: 'Brown', 4: 'Burgundy', 5: 'Gold', 6: 'Green',
                   7: 'Grey', 8: 'Khaki', 9: 'Multiple Colors', 10: 'Navy', 11: 'Orange', 12: 'Pink',
                   13: 'Purple', 14: 'Red', 15: 'Silver', 16: 'Transparent', 17: 'White', 18: 'Yellow'}


class ColourExtractorStrict:
    """
    Class to handle color extraction.
    """

    def __init__(self, colours):
        self.colours = colours
        self.pos_ok = ['ADJ', 'NOUN']
        self.tagger = spacy.load("ja_core_news_sm")

    def get(self, string):
        """
        Method that allows extracting the relevant parts of a string (i.e containing colors' names).

        Args:
            self : object
                the extractor object
            s : str
                the string from which relevant parts are extracted

        Return:
            A set of relavant substrings or boolean False if no relevant part has been detected
        """

        extracted = set()
        doc = self.tagger(string.lower())
        pairs = [(word.text, word.pos_) for word in doc]
        for index, pair in enumerate(pairs):
            text, pos = pair
            if text in self.colours:
                text_ahead = self.look_ahead(pairs=pairs, index=index)
                text_behind = self.look_behind(pairs=pairs, index=index,
                                               colour_pos=pos)
                if text_behind:
                    text_behind.append(text)
                    if text_ahead:
                        text_behind.extend(text_ahead)
                        extracted.add(' '.join(text_behind))
                    else:
                        extracted.add(' '.join(text_behind))
                elif text_ahead:
                    extracted.add(' '.join([text] + text_ahead))
                else:
                    extracted.add(text)

        return ''.join(list(extracted)) if extracted else ' '

    def look_ahead(self, pairs, index):
        ahead = list()
        for text, pos in pairs[index + 1:]:
            if pos in self.pos_ok:
                ahead.append(text)
            else:
                break

        return ahead if ahead else False

    def look_behind(self, pairs, index, colour_pos):
        behind = list()
        for text, pos in reversed(pairs[:index]):
            if pos in self.pos_ok:
                behind.append(text)
            else:
                break

        return list(reversed(behind)) if behind else False


def tokenize_text(text):
    """
    This tokenizes words using the fugashi or Mecab tokenizer (tokenizer for japanese).

    Args:
        text : str
            the text to tokenize

    Return:
        components : list
            a list containing the tokenized words of the text
    """

    text = preprocess_ja(text)
    mt = MeCab.Tagger()

    mt.parse('')

    parsed = mt.parseToNode(text)
    components = []

    while parsed:
        components.append(parsed.surface)
        parsed = parsed.next

    return components


def encode_labels(labels):
    """
    This allows encoding the labels in a One-Hot vector format.

    ex: ['Black', 'White'] becomes [0, 1, 0, ..., 0, 1, 0, ..., 0], a numpy array of size 19 (nb of classes)
    """

    labels = labels.apply(lambda x: set(ast.literal_eval(x)))
    mlb = MultiLabelBinarizer()  # Multi-Label One-Hot-Encoder
    labels_encoded = mlb.fit_transform(labels.values.ravel())
    return labels_encoded


def ont_hot_decoder(colors_encoded):
    """
    This allows retrieving an array of colors from one hot encoded vector predictions.

    Args:
        colors_encoded : PyTorch tensor
            a NumPy array containing the encoded color predictions

    Return:
        a NumPy array with the decoded vector
        ex: [0, 0, ..., 1, 0, ..., 1, 0, ...] becomes ['Black', 'White']
    """
    colors_decoded = []
    for pred in colors_encoded:
        indexes = np.where(pred == 1)[0].tolist()
        colors = str(list(map(index_to_colors.get, indexes)))
        colors_decoded.append(colors)
    colors_decoded = np.array(colors_decoded)

    return colors_decoded


def preprocess_ja(s):
    """
    This allows processessing a Japanese string by removing non-word characters and separating tokens with spaces.
    """

    s = s.strip()
    s = re.sub(r"[^\w.!?\u3002]+", r" ", s, flags=re.UNICODE)
    s = re.sub(r'[^\w\s]', '', s)
    s = re.sub("^\d+\s|\s\d+\s|\s\d+$", " ", s)
    s = re.sub(r'[0-9]+', '', s)
    s = re.sub("\s+", " ", s).strip()
    s = re.sub(r'[A-Za-z]+', '', s)
    return s


def get_Embedding(text):
    """
    Creates words Embedding using genism fasttext model
    """
    tokenized_text = tokenize_text(text)
    return torch.Tensor([ft.get_word_vector(x) for x in tokenized_text])


extractor = ColourExtractorStrict(colours=colors_list_ja)


def extract_colors(s):
    """
    This allows extracting the relevant parts of a string (i.e containing colors' names).

    Args:
        s : str
            the string from which relevant parts are extracted

    Return:
        A set of relavant substrings or boolean False if no relevant part has been detected
    """

    return extractor.get(str(str(s)))


def process_text(dataset_dir=None):
    """
    This allows preprocessing the textual data and saving the resulting DataFrame in a csv file

    Args:
        dataset_dir : str
            the path to the directory containing the DataFrame of item names and captions
    """

    if not dataset_dir:
        dataset_dir = "../Data"

    # Loading the captions and names DataFrame for the test set
    test_capnames_df = pd.read_csv(os.path.join(dataset_dir, 'X_test.csv'), index_col=0)
    # Preprocessing the names and captions
    test_capnames_df['item_caption'] = test_capnames_df['item_caption'].fillna(value='<unk>')  # Filling missing values
    test_capnames_df['item_name_processed'] = test_capnames_df['item_name'].apply(preprocess_ja)  # Preprocessing the names
    test_capnames_df['item_caption_processed'] = test_capnames_df['item_caption'].apply(preprocess_ja)  # Preprocessing the captions
    # Concatenating the names and the captions
    test_capnames_df['capname'] = test_capnames_df['item_name_processed'] + '. ' + test_capnames_df['item_caption_processed']
    # Extracting the relevant colors/parts of the names+captions
    test_capnames_df['colors_extracted'] = test_capnames_df['capname'].apply(extract_colors)
    # Getting the lens of the colors_extracted strings from the captions/names
    test_capnames_df['lens'] = test_capnames_df['colors_extracted'].apply(lambda x: len(x))
    # Drop the useless data
    test_capnames_df.drop(['item_name_processed', 'item_caption_processed', 'item_caption',
                           'item_name', 'capname'], axis=1, inplace=True)
    # Saving the newly created DataFrame
    test_capnames_df.to_csv(os.path.join(dataset_dir, 'X_test_preprocessed.csv'))

    # Loading the captions and names DataFrame for the training/validation sets
    train_valid_capnames_df = pd.read_csv(os.path.join(dataset_dir, 'X_train.csv'), index_col=0)  # pandas DataFrame
    # Preprocessing the names and captions
    train_valid_capnames_df['item_caption'] = train_valid_capnames_df['item_caption'].fillna(value='<unk>')  # Filling missing values
    train_valid_capnames_df['item_name_processed'] = train_valid_capnames_df['item_name'].apply(preprocess_ja)  # Preprocessing the names
    train_valid_capnames_df['item_caption_processed'] = train_valid_capnames_df['item_caption'].apply(preprocess_ja)  # Preprocessing the captions
    # Concatenating the names and the captions
    train_valid_capnames_df['capname'] = train_valid_capnames_df['item_name_processed'] + '. ' + train_valid_capnames_df['item_caption_processed']
    # Extracting the relevant colors/parts of the names+captions
    train_valid_capnames_df['colors_extracted'] = train_valid_capnames_df['capname'].apply(extract_colors)
    # Getting the lens of the colors_extracted strings from the captions/names
    train_valid_capnames_df['lens'] = train_valid_capnames_df['colors_extracted'].apply(lambda x: len(x))
    # Drop the useless data
    train_valid_capnames_df.drop(['item_name_processed', 'item_caption_processed', 'item_caption',
                                 'item_name', 'capname'], axis=1, inplace=True)
    # Saving the newly created DataFrame
    train_valid_capnames_df.to_csv(os.path.join(dataset_dir, 'X_train_preprocessed.csv'))

    return


def generate_unique_logpath(logdir, raw_run_name):
    """
    This allows generating unique logpaths to save the model checkpoints during the trains.

    Args:
        logdir : str
            the path to the directory that will contain the logs
        raw_run_name : str
            the generic filename to use to save the logs

    Return:
        log_path : str
            the unique path to the file where to save the logs
    """

    i = 0
    while(True):
        run_name = raw_run_name + "_" + str(i)
        log_path = os.path.join(logdir, run_name)
        if not os.path.isdir(log_path):
            return log_path
        i = i + 1


def get_summary(dataset_desc, model, optimizer, others={}):
    """
    This allows generating a summary report during the training step.
    """

    summary_text = """

    Dataset
    =======
    {}

    Model summary
    =============
    {}

    {} Model trainable parameters

    Model Optimizer
    ========
    {}
    """.format(dataset_desc, model, sum(p.numel() for p in model.parameters() if p.requires_grad), optimizer)

    if len(others) > 0:
        summary_text += """

    Other parameters
    ======="""

        for key in others:
            summary_text += "    " + key + ": " + str(others[key]) + "\n"

    return summary_text


class ModelCheckpoint:
    """
    Class to handle model checkpoints generation.
    """

    def __init__(self, filepath, model):
        self.max_score = None
        self.filepath = filepath
        self.model = model

    def update(self, score):
        if (self.max_score is None) or (score > self.max_score):
            print("Saving a better model")
            torch.save(self.model.state_dict(), self.filepath)  # Saving only the model weights
            # torch.save(self.model, self.filepath)
            self.max_score = score
