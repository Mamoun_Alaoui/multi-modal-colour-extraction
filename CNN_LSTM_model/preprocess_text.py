from utils import process_text

"""
Script to be executed in order to generate the preprocessed textual data.
"""

if __name__ == '__main__':
    process_text()
