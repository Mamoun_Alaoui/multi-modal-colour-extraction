# Rakuten Multi-modal Colour Extraction

## Objectives of the challenge

**Building a model able to predict the colors of objects in a catalog**

This challenge focusses on prediction of colour attribute of products from a large-scale multimodal (text and image) e-commerce product catalog data of Rakuten Ichiba marketplace.

---

For more information, please refer to the [official ENS challenge website](https://challengedata.ens.fr/challenges/59).

---


## Models used

The goal of this section if to give a quick overview of the three models used for the challenge.

### Baseline model (ResNet18 with a custom linear layer)

This model mainly consist in using a pre-trained ResNet18 model where the **last layer have been replaced by a fully connected layer with an output dimension of 19 (number of final colors)**.

Since the classification task considered here is multi-label, one uses a sigmoid layer after the fully connected layer in order to output 19 target probabilities that will be thresholded to determine whether the color is present or not. The threshold used is 0.5.


![ResNet model](./imgs_for_git/resnet.png)


### CNN-LSTM model

In this model, one concatenates the output of a ResNet18 on image data with the output of a single layer LSTM dealing with textual input. The last layers are respectively:

- a dense layer casting the concatenated vector in a vector of size 19
- a sigmoid layer to output 19 probabilities.

![CNN-LSTM network](./imgs_for_git/CNN_LSTM.png)

### Attention based Encoder-Decoder model

This model is inspired by the [_Show, Attend, and Tell_](https://arxiv.org/abs/1502.03044) paper. The authors' original implementation can be found [here](https://github.com/kelvinxu/arctic-captions).

The idea is to let the model learn _where_ to look for colors.

This model learns _where_ to look.

Depending of the image considered and the name/desctiption provided, the model will gaze at different locations on the image.

This is possible because of an _Attention_ mechanism, which allows the model to focus on the relevant parts of the image, given its textual desctiption.

![Example of an attention mechanism](./imgs_for_git/attention_ex.png)

This model actually breaks down in three parts:

- an encoder that encodes the input image in a 2048-channels image of size (14, 14).
- a decoder that takes as an input some textual data, as well as the output of the encoder (as an initial hidden state). The decoder then decodes the input sentence to predict the colors.
- as said before, the model is enhanced by an attention mechanism that tells to the decoder where to focus on the image for each word of the sentence. The idea is to help the model to focus on relevant part of the input image given the descruption.

#### Encoder

![Encoder](./imgs_for_git/encoder.png)

The idea behind the encoder is to **encode the input image with 3 color channels into a smaller learned representation that contains all that is useful in the original image**.

Since we are dealing with image data, we use Convolutional Neural Networks (CNNs). More specifically, since the objects considered here are daily-life objects (tee-shirts, footballs, TV etc.), some pretrained models that detect features such as the edges, the colors etc. already exist.

The model chosen here is the **18 layered Residual Network trained on the ImageNet classification task**, already available in PyTorch. The last two layers are removed and replaced by an Adaptive average pool layer, that outputs an encoded representation of the image of size (2048, 14, 14).

#### Decoder

The goal of the decoder is to **look at the encoded image and decode the name+caption to a set of colors**.

Since it's decoding a sequence, one uses a Recurrent Neural Network (RNN). To avoid vanishing gradient issues, one uses an LSTM architecture.

![LSTM architecture](./imgs_for_git/lstm.svg)

At each step, the decoder takes as an input a word in the sequence of colors extracted from the captions and names, as well as the hidden state of the previous LSTM cell.
To take into account the image data, one feeds the encoded image representation (output of the encoder) to the decoder, as the initial hidden state.
After decoding the whole sequence of words, the decoder outputs a last hidden state that is fed to a dense layer then to a sigmoid layer. The final output is a vector of size 19 containing the probabilities of each class.

![Decoder](./imgs_for_git/decoder.png)


#### Attention mechanism

While using Attention, we want the Decoder to be able to **look at different parts of the image at different points in the sequence**. For example, if the decoder is reading the word "red" in the sentence, it must focus on the part of the image where the object is red.

In a model without attention, as an input of the decoder, one could simply average the encoded image and feed it as the initial hidden state of the LSTM network.

Here we use the _weighted_ average across all pixels, with the weights of the important pixels being greater. The attention works like a mask applied to the encoded image (and subsequently to the initial image).

The Attention network is responsible to **compute these weights**.

![Attention](./imgs_for_git/att_weights.png)

We will use _soft_ Attention, with weights adding up to 1. If there are `P` pixels in our encoded image, then at each timestep `t`

![Attention mechanism](./imgs_for_git/attention.png)


#### Final model

![The encoder-decoder model](./imgs_for_git/final_model.png)

## Implementation

### Requirements

#### Preprocessing the data, with the file preprocess_text.py
Color extraction of the initial dataset to keep only the relevant part of our dataset. Before that, we have to install differents packages: 
- Spacy, a japanese language model:

`pip install -U pip setuptools wheel`

`pip install -U spacy`

`python -m spacy download ja_core_news_sm`


#### Embedding layer with the file data.py

The file data.py preprocess the data that we have. Before that, we have to install differents packages: 

- Mecab, a Japanese tokenizer :

`pip3 install mecab-python3`

`pip3 install unidic-lite`

- FastText, an embedding model : 

` pip3 install fasttext`

- Transformers which contains Bert, an embedding model : 

`pip install transformers`

- PIL to deal with truncatured images:

`pip3 install Pillow`

### Dataset

For this challenge, Rakuten is releasing approx. 250K item listings in CSV format, including the train (212,659) and test set (37,528).
Files are accessible when logged in and registered to the challenge. You can register to the "Challenge Data ENS" [here](https://challengedata.ens.fr/register)
The data are divided under two criteria, forming four distinct sets: training or test, input or output.
- X_train.csv: training input file
- Y_train.csv: training output file
- X_test.csv: test input file
- images.zip file is supplied containing all the images. Uncompressing this file will provide a folder named images with all the item images.
The first line of all the files contains the header, and the columns are separated by comma (',').

The columns of the input files (X_train.csv and X_test.csv) are:
- image_file_name - The name of the image file in images folder corresponding to the item.
- item_name - The item title, a short text summarizing the item.
- item_caption - A more detailed text describing the item. Not all the merchants use this field, so to retain originality of the data, the description field can contain NaN value for many products.

### Inputs to model 

We will need four inputs.
*Note*: for the baseline model, only the images and encoded labels are needed. 

#### Images

We need to preprocess our images before passing it through our models.
We will resize all Rakuten images to 255x255 for uniformity.
We also add data transformation to the preprocess : axial symmetries, rotations and translations. 

#### Text
The initial dataset have item name and item caption. 
We first concatenate the two of them. Then we pass it through a Color extractor module, that permits to extract only the relevant informations that contains the colors. This step permits to reduce the length without loosing any relevant information.

*Note*: This step take time, so we provide the preprocessed dataset on the git (file Preprocessed_text)

#### Text length

Since the preprocessed texts are padded, we would need to keep track of the lengths of each one.

Therefore, **caption lengths fed to the model must be an `Int` tensor of dimension `N`**.

#### Encoded labels

Since it is a classification problem, we have to fed the model with the right labels to train it.
The initial labels on the Y_train are a list of colors. So we have to one hot encode it before feeding it to the model. To do so, one uses the [`encode_labels`](./Encoder-Decoder-Attention/utils.py) function in utils.py of every model.
To retrive a list of colors, one can use the [`one_hot_decoder`](./Encoder-Decoder-Attention/utils.py) function.


### Data pipeline


#### Text preprocessing

See `process_text()` in [`utils.py`](./Encoder-Decoder-Attention/utils.py).
To create clean input files for the textual data, one must launch the preprocess_text.py script. The preprocessing takes generally up to 2 hours.
In order to save you some time, the preprocessed text has already been uploaded in the git in the Preprocessed_text section. It consists in two csv files X_train_preprocessed.csv and X_test_preprocessed.csv which are the eact replicas of the initial dataframes, but with a column "colors_extracted" replacing the columns "item_name" and "item_caption".

The function `process_text()` in [`utils.py`](./Encoder-Decoder-Attention/utils.py) works as follows:
- missing captions are replaced by an 'unk' token.
- captions and names are concatenated.
- the captions are then processed by a color extractor which extract the part of the sentence where a color is mentionned. This extractor relies on a japanese tokenizer ([`MeCab`](https://en.wikipedia.org/wiki/MeCab) tokenizer). Indeed, unlike other languages like English or French, words are not separated by spaces in japanese. One must therefore use a specific tokenizer.


#### Image preprocessing

The images are resized to (255, 255) using the PIL package in python. They are also converted to the RGB format since there are few examples which are not already RGB.
During the train, some random transformations are applied to the image such as random flips and random roations. One must only ensures that no color-related transformation is used.


### First model (Baseline)

See `Net` in [`models.py`](h./Baseline_model/models.py).

We use a pretrained ResNet-18 already available in PyTorch's `torchvision` module. We simply replace the last layer by our own fully connected layer, followed by a sigmoid.
The reason why we use a sigmoid and not a softmax (despite the fact we have multiple classes) is because of the multi label classification. Indeed, instaed of outputting one single class, we want to output the probabilities for each class. Thus, it makes sense outputting a vector of 19 logits at the end of the model.

### Second model (CNN with LSTM)

See `ImageNet`  and `TextNet` in [`models.py`](./CNN_LSTM_model/models.py). Same as before, we used a ResNet-18 for the ImageNet part. The only difference is that we get a 1024 flattened tensor for each image as an output of the model. This tensor will be concatenated with the last hidden state of the LSTM.

For the text part, we used a uni-directional LSTM with a single layer. There is definitely space for improvment here (adding a second layer to the LSTM for instance, or using a bi-directional LSTM).
At the end of the LSTM, we use the last hidden layer (we use hidden_dim=512) and we concatenate it with the output of the ResNet.

After that, we feed the concatenated tensor to a dense layer and a sigmoid (same as before).

### Attention based Encoder-Decoder

#### Encoder

See `Encoder` in [`models.py`](./Encoder-Decoder-Attention/models.py).

We used a pretrained ResNet-18, where the last two layers have been removed (since we do not want to classify straight after the CNN).

We added an `AdaptiveAvgPool2d()` layer to **resize the encoding to a fixed size**.


#### Attention

See `Attention` in [`models.py`](./Encoder-Decoder-Attention/models.py).

The Attention network consists in linear layers and their activations.

The linear layers **transform the encoded image (flattened to `N, 14 * 14, 2048`) and the hidden state from the Decoder to the same dimension**. Following a concatenation and a ReLu activation, a third linear layer **transforms this result to a dimension of 1**. A **softmax is then applied to generate the weights** `alpha`.

#### Decoder

See `DecoderWithAttention` in [`models.py`](./Encoder-Decoder-Attention/models.py).

As said before, **the hidden and cell state of the LSTM** are initialized using the encoded image with the `init_hidden_state()` method.

The texts are already padded and sorted by decreasing lengths by the DataLoaders using the `collate_fn` argument in the torch DataLoaders. This is to prevent from processing the pads added to each sequence.

![](./imgs_for_git/sorted.jpeg)

### Training

Before you begin, make sure to save the required preprocessed text files. To do so, run the scripts [`preprocess_text.py`](./Encoder-Decoder-Attention/preprocess_text.py) or use the already created file in the Preprocessed_text section as said before.

The parameters for the three models are at the begining of the [`train.py`](./Encoder-Decoder-Attention/train.py). Feel free to modify it for your experiments.

To **train your model from scratch**, simply run the files –

`python3 train.py`

### Testing

To generate the prediction for the challenge after the training, make sure the path to the right best_mode.pt ie entered in the test.py function. Then, simply run the file –

`python3 test.py`

### Loss Function

Since wan to output a vector of 19 probabilities, we use the **[`BinaryCrossEntropyLoss or BCELoss`](https://pytorch.org/docs/stable/generated/torch.nn.BCELoss.html)**.


