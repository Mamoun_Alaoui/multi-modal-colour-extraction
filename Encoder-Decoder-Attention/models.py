import torch
from torch import nn
import torchvision
from transformers import BertModel, BertJapaneseTokenizer
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence, PackedSequence, pad_sequence

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Load pre-trained bert model (weights)
bert_model = BertModel.from_pretrained("../Embeddings/bert-base-japanese")

if torch.cuda.is_available():
    bert_model.to("cuda")


class Encoder(nn.Module):
    """
    Encoder.
    """

    def __init__(self, encoded_image_size=14):
        super(Encoder, self).__init__()
        self.enc_image_size = encoded_image_size

        resnet = torchvision.models.resnet101(pretrained=True)  # pretrained ImageNet ResNet-101

        # Remove linear and pool layers (since we're not doing the classification straight after the encoder)
        modules = list(resnet.children())[:-2]
        self.resnet = nn.Sequential(*modules)

        # Resize image to fixed size to allow input images of variable size
        self.adaptive_pool = nn.AdaptiveAvgPool2d((encoded_image_size, encoded_image_size))

    def forward(self, images):
        """
        Forward propagation.

        Args:
            images : PyTorch tensor
                a tensor of dimensions (batch_size, 3, image_size, image_size)

        Return:
            The encoded images
        """

        out = self.resnet(images)  # (batch_size, 2048, image_size/32, image_size/32)
        out = self.adaptive_pool(out)  # (batch_size, 2048, encoded_image_size, encoded_image_size)
        out = out.permute(0, 2, 3, 1)  # (batch_size, encoded_image_size, encoded_image_size, 2048)
        return out


class Attention(nn.Module):
    """
    The Attention Network.
    input: encoded image + previous output of the decoder
    output: encoded image weighted indicating where the decoder should pay attention
    """

    def __init__(self, encoder_dim, decoder_dim, attention_dim):
        """
        Args:
            encoder_dim : int
                feature size of encoded images
            decoder_dim : int
                size of decoder's RNN
            attention_dim : int
                size of the attention network
        """

        super(Attention, self).__init__()
        self.encoder_att = nn.Linear(encoder_dim, attention_dim)  # linear layer to transform encoded image
        self.decoder_att = nn.Linear(decoder_dim, attention_dim)  # linear layer to transform decoder's output
        self.full_att = nn.Linear(attention_dim, 1)  # linear layer to calculate values to be softmax-ed
        self.relu = nn.ReLU()
        self.softmax = nn.Softmax(dim=1)  # softmax layer to calculate weights

    def forward(self, encoder_out, decoder_hidden):
        """
        Forward propagation.

        Args:
            encoder_out : PyTorch tensor
                encoded images, a tensor of dimension (batch_size, num_pixels, encoder_dim)
            decoder_hidden : PyTorch tensor
                previous decoder output, a tensor of dimension (batch_size, decoder_dim)

        Return:
            The weights of the attention model i.e attention weighted encodings
        """

        att1 = self.encoder_att(encoder_out)  # (batch_size, num_pixels, attention_dim)
        att2 = self.decoder_att(decoder_hidden)  # (batch_size, attention_dim)
        att = self.full_att(self.relu(att1 + att2.unsqueeze(1))).squeeze(2)  # (batch_size, num_pixels)
        alpha = self.softmax(att)  # (batch_size, num_pixels)
        attention_weighted_encoding = (encoder_out * alpha.unsqueeze(2)).sum(dim=1)  # (batch_size, encoder_dim)

        return attention_weighted_encoding, alpha


class DecoderWithAttention(nn.Module):
    """
    Decoder.
    input : previous hidden state (for the first one, output of the encoder)
            weighted encoded images from the attention
    output : hidden state , one for the next decoder and one for the attention
    """

    def __init__(self, attention_dim, decoder_dim, vocab_size, nb_classes,
                 encoder_dim=2048, dropout=0.5, embedding_method='fasttext', embed_dim=None):
        """
        Args:
            attention_dim : int
                size of attention network
            decoder_dim : int
                size of decoder's RNN
            encoder_dim : int
                feature size of encoded images
            nb_classes : int
                number of color classes to predict (ex: 19 in this challenge)
            dropout : float
                dropout ratio
            embedding_method : str
                embedding method used. One of ['fasttext', 'bert', 'other']
            embed_dim : int
                embedding size (optional, only if embedding_method != 'fasttext' or 'bert')
            vocab_size : int
                size of the vocabulary used (optional, only if embedding_method != 'fasttext' or 'bert')
        """

        super(DecoderWithAttention, self).__init__()

        self.encoder_dim = encoder_dim
        self.attention_dim = attention_dim
        self.decoder_dim = decoder_dim
        self.nb_classes = nb_classes
        self.dropout = dropout
        self.embedding_method = embedding_method
        self.vocab_size = vocab_size

        self.attention = Attention(encoder_dim, decoder_dim, attention_dim)  # Attention network

        if self.embedding_method == 'bert':
            # Load pre-trained bert model (weights)
            self.embedding = BertModel.from_pretrained("../Embeddings/bert-base-japanese")
            self.embed_dim = 768
        elif self.embedding_method == 'fasttext':
            self.embed_dim = 300
        else:
            if embed_dim is not None:
                self.embedding = nn.Embedding(vocab_size, embed_dim)  # embedding layer
            else:
                raise ValueError('embedding dim must be provided')

        self.dropout = nn.Dropout(p=self.dropout)
        self.decode_step = nn.LSTMCell(self.embed_dim + encoder_dim, decoder_dim, bias=True)  # decoding LSTMCell
        self.init_h = nn.Linear(encoder_dim, decoder_dim)  # linear layer to find initial hidden state of LSTMCell
        self.init_c = nn.Linear(encoder_dim, decoder_dim)  # linear layer to find initial cell state of LSTMCell
        self.f_beta = nn.Linear(decoder_dim, encoder_dim)  # linear layer to create a sigmoid-activated gate

        self.fc = nn.Linear(decoder_dim, nb_classes)  # linear layer to find scores over our multiclasses
        self.sigmoid = nn.Sigmoid()

        # self.init_weights()  # initialize some layers with the uniform distribution

        self.sigm = nn.Sigmoid()

    def init_weights(self):
        """
        Initializes some parameters with values from the uniform distribution, for easier convergence.
        """
        self.embedding.weight.data.uniform_(-0.1, 0.1)

    def load_pretrained_embeddings(self, embeddings):
        """
        Loads embedding layer with pre-trained embeddings.

        Args:
            embeddings : object (dict of weigths)
                pre-trained embeddings
        """
        self.embedding.weight = nn.Parameter(embeddings)

    def fine_tune_embeddings(self, fine_tune=True):
        """
        Allow fine-tuning of embedding layer? (Only makes sense to not-allow if using pre-trained embeddings).

        Args:
            fine_tune : bool
                Allow ?
        """
        for p in self.embedding.parameters():
            p.requires_grad = fine_tune

    def init_hidden_state(self, encoder_out):
        """
        Creates the initial hidden and cell states for the decoder's LSTM based on the encoded images.

        Args:
            encoder_out : PyTorch tensor
                encoded images, a tensor of dimension (batch_size, num_pixels, encoder_dim)

        Return:
            hidden state, cell state
        """

        mean_encoder_out = encoder_out.mean(dim=1)
        h = self.init_h(mean_encoder_out)  # (batch_size, decoder_dim)
        c = self.init_c(mean_encoder_out)
        return h, c

    def forward(self, encoder_out, encoded_text, lens):
        """
        Forward propagation.

        Args:
            encoder_out : PyTorch tensor
                encoded images, a tensor of dimension (batch_size, num_pixels, encoder_dim)
            encoded_text : PyTorch tensor
                a PyTorch tensor containing the preprocessed textual data, either tokenized
                (if embedding_method == 'bert' or 'other') or directly embedded (if 'fasttext')
            lens : PyTorch tensor
                a PyTorch tensor containing the lens of the preprocessed textual data

        Return:
            a vector of scores for each color class, sorted embedded textual data, decode lengths, weights
        """

        batch_size = encoder_out.size(0)
        encoder_dim = encoder_out.size(-1)

        # Flatten image
        encoder_out = encoder_out.view(batch_size, -1, encoder_dim)  # (batch_size, num_pixels, encoder_dim)
        num_pixels = encoder_out.size(1)

        # Embedding
        if self.embedding_method == 'bert':
            with torch.no_grad():
                embeddings = self.embedding(**encoded_text)[0]  # (batch_size, max_caption_length, embed_dim)
        elif self.embedding_method == "fasttext":
            embeddings = encoded_text
        else:
            embeddings = self.embedding(encoded_text)

        # Unpack the padded embeddings to feed it to the model
        unpacked_data, lens_outputs = pad_packed_sequence(embeddings, batch_first=True)

        # Initialize LSTM state
        h, c = self.init_hidden_state(encoder_out)  # (batch_size, decoder_dim)

        # Create tensors to hold alphas
        alphas = torch.zeros((batch_size, torch.max(lens).item(), num_pixels)).to(device)
        pred_final = torch.zeros((batch_size, self.nb_classes)).to(device)

        # At each time-step, decode by
        # attention-weighing the encoder's output based on the decoder's previous hidden state output
        batch_size_t = batch_size
        for t in range(torch.max(lens).item()):
            batch_size_prev = batch_size_t
            batch_size_t = sum([length > t for length in lens])
            nb_disappear = batch_size_prev - batch_size_t
            attention_weighted_encoding, alpha = self.attention(encoder_out[:batch_size_t],
                                                                h[:batch_size_t])
            gate = self.sigmoid(self.f_beta(h[:batch_size_t]))  # gating scalar, (batch_size_t, encoder_dim)
            attention_weighted_encoding = gate * attention_weighted_encoding  # Passing the attention-weighted through a gate
            pred = self.fc(self.dropout(h[batch_size_t:batch_size_t + nb_disappear]))  # (batch_size_t, nb_classes)
            pred_final[batch_size_t:batch_size_t + nb_disappear, :] = pred
            h, c = self.decode_step(
                torch.cat([unpacked_data[:batch_size_t, t, :], attention_weighted_encoding], dim=1),
                (h[:batch_size_t], c[:batch_size_t]))  # (batch_size_t, decoder_dim)
            alphas[:batch_size_t, t, :] = alpha

        out = self.sigm(pred_final)            # (batch_size_t, nb_classes)
        return out, encoded_text, lens, alphas


class Net(nn.Module):
    """
    Final model (encoder-decoder combination).
    input : images, text
    output : hidden state , one for the next decoder and one for the attention
    """

    def __init__(self, attention_dim, decoder_dim, vocab_size, nb_classes,
                 encoder_dim=2048, dropout=0.5, embedding_method='fasttext', embed_dim=None):
        """
        Args:
            attention_dim : int
                size of attention network
            decoder_dim : int
                size of decoder's RNN
            encoder_dim : int
                feature size of encoded images
            nb_classes : int
                number of color classes to predict (ex: 19 in this challenge)
            dropout : float
                dropout ratio
            embedding_method : str
                embedding method used. One of ['fasttext', 'bert', 'other']
            embed_dim : int
                embedding size (optional, only if embedding_method != 'fasttext' or 'bert')
            vocab_size : int
                size of the vocabulary used (optional, only if embedding_method != 'fasttext' or 'bert')
        """

        super(Net, self).__init__()

        self.encoder = Encoder()
        self.decoder = DecoderWithAttention(attention_dim, decoder_dim, vocab_size, nb_classes,
                                            encoder_dim, dropout, embedding_method, embed_dim)

    def forward(self, img, encoded_text, lens):
        """
        Forward propagation.

        Args:
            img : PyTorch tensor
                initial images to be feeded to the encoder
            encoded_text : PyTorch tensor
                a PyTorch tensor containing the preprocessed textual data, either tokenized
                (if embedding_method == 'bert' or 'other') or directly embedded (if 'fasttext')
            lens : PyTorch tensor
                a PyTorch tensor containing the lens of the preprocessed textual data

        Return:
            a vector of scores for each color class, sorted embedded textual data, decode lengths, weights
        """

        img_encoded = self.encoder(img)
        out, encoded_text, lens, alphas = self.decoder(img_encoded, encoded_text, lens)

        return out
