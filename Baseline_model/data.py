import torch
import os
import pandas as pd
import numpy as np
import tqdm

from PIL import Image, ImageFile
import torchvision.transforms as transforms
from transformers import BertModel, BertJapaneseTokenizer  # Bert embedding model
import MeCab  # Japanese tokenizer
import fasttext.util  # Fasttext embedding model
from torch.nn.utils.rnn import pad_sequence, pack_padded_sequence  # To pad and pack sequences
import utils

ImageFile.LOAD_TRUNCATED_IMAGES = True  # To deal with truncated images


class ColorsDataset(torch.utils.data.Dataset):
    """
    A PyTorch Dataset class to be used in a PyTorch DataLoader to create batches.
    """

    def __init__(self, images_dir, text_data_df, encoded_labels=None):
        """
        This constructs a PyTorch Dataset object from the data.

        Args:
            self : the Dataset object
            images_dir : str
                the path to the directory containing the image data
            text_data_df : pandas DataFrame
                the pandas DataFrame containing the colors extracted from the captions+names and theirs lengths
            encoded_labels : NumPy array
                the NumPy array containing the encoded labels

        Return:
            The PyTorch Dataset object constructed
        """

        self.images_dir = images_dir
        self.text_data_df = text_data_df
        if encoded_labels is not None:
            self.encoded_labels = encoded_labels
        else:
            self.encoded_labels = None

        self.dataset_size = len(self.text_data_df)

    def __getitem__(self, index):
        img_file = self.text_data_df['image_file_name'][index]
        img = Image.open(os.path.join(self.images_dir, self.text_data_df['image_file_name'][index]))
        img = img.convert('RGB')  # Convert the images to the RGB format
        if self.encoded_labels is not None:
            encoded_label = torch.Tensor(self.encoded_labels[index])
            return img, encoded_label

        return img

    def __len__(self):
        return self.dataset_size


class DatasetTransformer(torch.utils.data.Dataset):
    """
    A PyTorch Dataset class to handle data transformation.
    """

    def __init__(self, base_dataset, transform, split):
        self.base_dataset = base_dataset
        self.transform = transform
        self.split = split

    def __getitem__(self, index):
        if self.split in ['train', 'valid']:
            img, encoded_label = self.base_dataset[index]
            return self.transform(img), encoded_label
        else:
            img = self.base_dataset[index]
            return self.transform(img)

    def __len__(self):
        return len(self.base_dataset)


def load_colors_datasets(valid_ratio, batch_size,
                         num_workers, images_dir=None,
                         dataset_dir=None, train_augment_transforms=None):
    """
    This allows loading the DataLoaders for the train, validation and test sets.

    Args:
        valid_ratio : float
            the ratio of samples in the validation set
        batch_size : int
            the batch size to use
        num_workers : int
            the number of cpu cores to use during the DataLoaders creation
        dataset_dir : str
            the path to the directory containing the input data
        images_dir : str
            the path to the specific directory containing the image input data
        train_augment_transforms : object
            list of transformation to be applied on the training set images

    Return:
        the train, valid and test DataLoaders + the vocabulary created from the data (optional)
    """

    if not dataset_dir:
        dataset_dir = "../Data"

    if not images_dir:
        images_dir = os.path.join(dataset_dir, 'images')

    # Loading the DataFrame containing the preprocessed textual data for the train/validation set
    train_valid_text_df = pd.read_csv(os.path.join(dataset_dir, 'X_train_preprocessed.csv'), index_col=0)  # pandas DataFrame
    # Loading the DataFrame containing the labels and encoding it
    train_valid_labels_df = pd.read_csv(os.path.join(dataset_dir, 'y_train.csv'), index_col=0)  # pandas DataFrame
    train_valid_labels_encoded = utils.encode_labels(train_valid_labels_df.color_tags)  # NumPy array

    # Loading the DataFrame containing the preprocessed textual data for the text set
    test_text_df = pd.read_csv(os.path.join(dataset_dir, 'X_test_preprocessed.csv'), index_col=0)

    train_valid_dataset = ColorsDataset(images_dir, train_valid_text_df, train_valid_labels_encoded)  # PyTorch Dataset
    test_dataset = ColorsDataset(images_dir, test_text_df)  # PyTorch Dataset

    # Split the train_valid dataset into training and validation sets
    nb_train, nb_valid = int((1.0 - valid_ratio) * len(train_valid_dataset)), int(valid_ratio * len(train_valid_dataset))
    train_dataset, valid_dataset = torch.utils.data.dataset.random_split(train_valid_dataset, [nb_train, nb_valid])

    # Use a dict for data transformation
    data_transforms = {'train': transforms.Compose([transforms.ToTensor(), transforms.Resize((255, 255))]),
                       'valid': transforms.Compose([transforms.ToTensor(), transforms.Resize((255, 255))]),
                       'test': transforms.Compose([transforms.ToTensor(), transforms.Resize((255, 255))])}

    # Add the data transformations to the train data_transforms dict
    if train_augment_transforms:
        data_transforms['train'] = transforms.Compose([train_augment_transforms, transforms.ToTensor()])

    # Applying the transformations
    train_dataset = DatasetTransformer(train_dataset, data_transforms['train'], split='train')
    valid_dataset = DatasetTransformer(valid_dataset, data_transforms['valid'], split='valid')
    test_dataset = DatasetTransformer(test_dataset, data_transforms['test'], split='test')

    # shuffle = True : reshuffles the data at every epoch
    train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                               batch_size=batch_size,
                                               shuffle=True,
                                               num_workers=num_workers)

    valid_loader = torch.utils.data.DataLoader(dataset=valid_dataset,
                                               batch_size=batch_size,
                                               shuffle=True,
                                               num_workers=num_workers)

    test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                              batch_size=batch_size,
                                              shuffle=False,
                                              num_workers=num_workers)

    return train_loader, valid_loader, test_loader
