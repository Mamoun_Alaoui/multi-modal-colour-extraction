import torch
import ast
import os
import pandas as pd
import numpy as np

import re
from sklearn.preprocessing import MultiLabelBinarizer
import MeCab
import fasttext.util
import spacy  # Download the japanese spacy language model: python -m spacy download ja_core_news_sm or python3 -m etc.

# The list of colors used to extract relevant part of the names + captions
colors_list_ja = ['イエロー', 'オレンジ', 'カーキ', 'グリーン', 'グレー', 'ゴールド', 'シルバー', 'ネイビー',
                  'パープル', 'ピンク', 'ブラウン', 'ブラック', 'ブルゴーニュ', 'ブルー', 'ベージュ', 'ホワイト',
                  'レッド', 'ワインレッド', '白い', '紫の', '緑', '複数の色', '褐色', '赤', '透明性', '銀', '青い',
                  '黄', '黒']

# Useful dict to extract the colors from one hot vectors
index_to_colors = {0: 'Beige', 1: 'Black', 2: 'Blue', 3: 'Brown', 4: 'Burgundy', 5: 'Gold', 6: 'Green',
                   7: 'Grey', 8: 'Khaki', 9: 'Multiple Colors', 10: 'Navy', 11: 'Orange', 12: 'Pink',
                   13: 'Purple', 14: 'Red', 15: 'Silver', 16: 'Transparent', 17: 'White', 18: 'Yellow'}


def encode_labels(labels):
    """
    This allows encoding the labels in a One-Hot vector format.

    ex: ['Black', 'White'] becomes [0, 1, 0, ..., 0, 1, 0, ..., 0], a numpy array of size 19 (nb of classes)
    """

    labels = labels.apply(lambda x: set(ast.literal_eval(x)))
    mlb = MultiLabelBinarizer()  # Multi-Label One-Hot-Encoder
    labels_encoded = mlb.fit_transform(labels.values.ravel())
    return labels_encoded


def ont_hot_decoder(colors_encoded):
    """
    This allows retrieving an array of colors from one hot encoded vector predictions.

    Args:
        colors_encoded : PyTorch tensor
            a NumPy array containing the encoded color predictions

    Return:
        a NumPy array with the decoded vector
        ex: [0, 0, ..., 1, 0, ..., 1, 0, ...] becomes ['Black', 'White']
    """
    colors_decoded = []
    for pred in colors_encoded:
        indexes = np.where(pred == 1)[0].tolist()
        colors = str(list(map(index_to_colors.get, indexes)))
        colors_decoded.append(colors)
    colors_decoded = np.array(colors_decoded)

    return colors_decoded


def generate_unique_logpath(logdir, raw_run_name):
    """
    This allows generating unique logpaths to save the model checkpoints during the trains.

    Args:
        logdir : str
            the path to the directory that will contain the logs
        raw_run_name : str
            the generic filename to use to save the logs

    Return:
        log_path : str
            the unique path to the file where to save the logs
    """

    i = 0
    while(True):
        run_name = raw_run_name + "_" + str(i)
        log_path = os.path.join(logdir, run_name)
        if not os.path.isdir(log_path):
            return log_path
        i = i + 1


def get_summary(dataset_desc, model, optimizer, others={}):
    """
    This allows generating a summary report during the training step.
    """

    summary_text = """

    Dataset
    =======
    {}

    Model summary
    =============
    {}

    {} Model trainable parameters

    Model Optimizer
    ========
    {}
    """.format(dataset_desc, model, sum(p.numel() for p in model.parameters() if p.requires_grad), optimizer)

    if len(others) > 0:
        summary_text += """

    Other parameters
    ======="""

        for key in others:
            summary_text += "    " + key + ": " + str(others[key]) + "\n"

    return summary_text


class ModelCheckpoint:
    """
    Class to handle model checkpoints generation.
    """

    def __init__(self, filepath, model):
        self.max_score = None
        self.filepath = filepath
        self.model = model

    def update(self, score):
        if (self.max_score is None) or (score > self.max_score):
            print("Saving a better model")
            torch.save(self.model.state_dict(), self.filepath)  # Saving only the model weights
            # torch.save(self.model, self.filepath)
            self.max_score = score
