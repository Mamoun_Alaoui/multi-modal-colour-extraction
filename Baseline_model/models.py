import torch
from torch import nn
import torchvision
from transformers import BertModel, BertJapaneseTokenizer
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence, PackedSequence, pad_sequence

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class Net(nn.Module):
    """
    Encoder for the images // pretrained : resnet without the fc at the end
    """

    def __init__(self, nb_classes=19, encoded_image_size=14):
        super(Net, self).__init__()
        self.nb_classes = nb_classes

        # model = torchvision.models.densenet121(pretrained=True)  # pretrained ImageNet DenseNet-121
        model = torchvision.models.resnet18(pretrained=True)  # pretrained ImageNet ResNet-18

        self.model = model

        fc_input_size = self.model.fc.in_features

        self.model.fc = nn.Linear(fc_input_size, nb_classes)

        self.sigm = nn.Sigmoid()

    def forward(self, img):
        """
        Forward propagation.
        """
        out = self.model(img)
        out = self.sigm(out)

        return out
