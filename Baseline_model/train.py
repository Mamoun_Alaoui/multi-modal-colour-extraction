import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import torchvision.transforms as transforms
from torch import nn
from torch.nn.utils.rnn import pack_padded_sequence
from torch.utils.tensorboard import SummaryWriter
from metrics import custom_metric_function
import pandas as pd

from models import Net
import data
import utils

import os
import sys
from tqdm import tqdm


# Model parameters
dropout = 0.5  # dropout ratio
nb_classes = 19  # 19 colors to classify
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")  # sets device for model and PyTorch tensors

# Training parameters
epochs = 20  # number of epochs to train
batch_size = 128  # size of minibatches
num_workers = 1  # number of cpu cores used for data-loading
lr = 0.001  # learning rate
valid_ratio = 0.2  # Going to use 80%/20% split for train/valid
logdir = utils.generate_unique_logpath("./logs", "rakuten_model")  # path to the directory to store the best models
step_size = 5  # step size for scheduler
gamma = 0.5  # param for scheduler


def main():
    """
    Training and validation.
    """

    tensorboard_writer = SummaryWriter(log_dir=logdir)

    train_augment_transforms = transforms.Compose([transforms.RandomAffine(degrees=(0, 360)),
                                                   transforms.RandomHorizontalFlip()])

    # import dataloaders
    train_loader, valid_loader, test_loader = data.load_colors_datasets(valid_ratio, batch_size,
                                                                        num_workers, images_dir=None,
                                                                        dataset_dir=None, train_augment_transforms=None)

    # Initialize models and optimizer
    net = Net(nb_classes=nb_classes)

    optimizer = torch.optim.Adam(params=filter(lambda p: p.requires_grad, net.parameters()),
                                 lr=lr)

    scheduler = torch.optim.lr_scheduler.StepLR(optimizer,
                                                step_size=step_size,
                                                gamma=gamma)

    # Move to GPU, if available
    net = net.to(device)

    # Loss function -- Cross entropy for multilabel
    criterion = nn.BCELoss().to(device)

    summary_text = utils.get_summary("Rakuten Colors Dataset", net, optimizer,
                                     others={"batch_size": batch_size})

    tensorboard_writer.add_text("Experiment summary", summary_text)

    # Define the callback objects
    net_checkpoint = utils.ModelCheckpoint(logdir + "/best_model.pt", net)

    # Epochs
    for epoch in range(epochs):
        print("Epoch {}".format(epoch))

        # One epoch's training
        train_loss = train(train_loader=train_loader,
                           model=net,
                           criterion=criterion,
                           optimizer=optimizer,
                           scheduler=scheduler,
                           epoch=epoch)

        print(" Training Loss : {:.4f}".format(train_loss))

        # One epoch's validation
        val_loss, f1_score = validate(val_loader=valid_loader, model=net, criterion=criterion)

        print(" Validation Loss : {:.4f}".format(val_loss))
        print(" F1 score : {}".format(f1_score))

        tensorboard_writer.add_scalar('metrics/train_loss', train_loss, epoch)
        tensorboard_writer.add_scalar('metrics/val_loss', val_loss, epoch)

        net_checkpoint.update(f1_score)


def train(train_loader, model, criterion, optimizer, scheduler, epoch):
    """
    Performs one epoch's training.
    """

    model.train()  # train mode (dropout and batchnorm is used)

    N = 0
    tot_loss = 0.0

    # Batches
    for i, (img, encoded_label) in enumerate(tqdm(train_loader)):

        # Move to GPU, if available
        img = img.to(device)
        encoded_label = encoded_label.to(device)

        # Forward prop.
        out = model(img)

        # Calculate loss
        loss = criterion(out, encoded_label.float())

        # Accumulate the number of processed samples
        N += img.shape[0]

        # For the total loss
        tot_loss += img.shape[0] * loss.item()

        # Back prop.
        model.zero_grad()
        optimizer.zero_grad()
        loss.backward()

        # Update weights
        optimizer.step()

    scheduler.step()

    return tot_loss / N


def validate(val_loader, model, criterion):
    """
    Performs one epoch's validation.
    """

    # explicitly disable gradient calculation to avoid CUDA memory error
    with torch.no_grad():
        model.eval()  # eval mode (no dropout or batchnorm)

        N = 0
        tot_loss = 0.0
        decoded_preds_arr = []
        true_labels_arr = []

        # Batches
        for i, (img, encoded_label) in enumerate(tqdm(val_loader)):

            # Move to device, if available
            img = img.to(device)
            encoded_label = encoded_label.to(device)

            # Forward prop.
            out = model(img)

            # Calculate loss
            loss = criterion(out, encoded_label.float())

            # Accumulate the number of processed samples
            N += img.shape[0]

            # For the total loss
            tot_loss += img.shape[0] * loss.item()

            # Compute predictions
            predicted_targets = torch.round(out)

            # Retrieve the lists of colors from the predictions
            decoded_preds_arr_batch = utils.ont_hot_decoder(predicted_targets.cpu().numpy()).tolist()
            decoded_preds_arr.extend(decoded_preds_arr_batch)

            # Retrieve the lists of colors from the encoded true labels
            true_labels_arr_batch = utils.ont_hot_decoder(encoded_label.cpu().numpy()).tolist()
            true_labels_arr.extend(true_labels_arr_batch)

        # Storing the results in a DataFrame
        result_df = pd.DataFrame()
        result_df['color_tags'] = decoded_preds_arr

        # Storing the true labels in a DataFrame
        true_df = pd.DataFrame()
        true_df['color_tags'] = true_labels_arr

        f1_score = custom_metric_function(true_df, result_df)

    return tot_loss / N, f1_score


if __name__ == '__main__':
    main()
